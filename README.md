# EcoLogic.ApiKeyAuthentication - User/Key API Authentication for asp.net core

EcoLogic.ApiKeyAuthentication allows to register an ApiKeyAuthenticationHandler into your asp.net core application.
The User/Key-Pairs used for the Basic-Auth mechanism are read from appsettings.

```csharp
//...
using EcoLogic.ApiKeyAuthentication.Extensions;
using EcoLogic.ApiKeyAuthentication.Handler;
using EcoLogic.ApiKeyAuthentication.Models;

namespace sp.api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; set; }

        public void ConfigureServices(IServiceCollection services)
        {
            //...
            services.AddEcoLogicApiKeyAuthentication();

        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILogger<Startup> logger, IServiceProvider serviceProvider)
        {

           //...
           services.Configure<List<ApiUserKeyCombination>>(options => Configuration.GetSection("ApiUserKeyCombinations").Bind(options));
           services
                .AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = ApiKeyAuthenticationHandler.AuthenticationScheme;
                    options.DefaultChallengeScheme = ApiKeyAuthenticationHandler.AuthenticationScheme;
                })
                .AddScheme<AuthenticationSchemeOptions, ApiKeyAuthenticationHandler>(ApiKeyAuthenticationHandler.AuthenticationScheme, ApiKeyAuthenticationHandler.AuthenticationScheme,
                    options => { });
        }
    }
}
```

appsettings.XYZ.json example

```json
{
  "ApiUserKeyCombinations": [
    {
      "UserId": "USER1234",
      "ApiKey": "NigjsgXw9},#H~@Z{ReV%Tg2I[?$UBZyMPB;M?8eVs/XWOvsNU3V4"
    },
    {
      "UserId": "USER2345",
      "ApiKey": "35NH$m^H.M^:`JlxX)MbTNF*.u9aE'A(3isF|IC1j^,hn0&E5XEOk"
    }
  ]
}
```

controller example:

```csharp
using EcoLogic.ApiKeyAuthentication.Handler;

//...

        [HttpPost]
        [Authorize(AuthenticationSchemes = ApiKeyAuthenticationHandler.AuthenticationScheme)]
        public ActionResult DoSomething()
        {
            return Ok();
        }

//...

```
