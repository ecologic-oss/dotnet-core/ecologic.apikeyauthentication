using EcoLogic.ApiKeyAuthentication.Handler;
using EcoLogic.ApiKeyAuthentication.KeySources;

namespace EcoLogic.ApiKeyAuthentication.Extensions
{
    using Microsoft.Extensions.DependencyInjection;

    public static class ApiKeyAuthenticationExtensions
    {
        public static IServiceCollection AddEcoLogicApiKeyAuthentication(this IServiceCollection services)
        {
            services.AddTransient<IApiKeyAuthenticationSource, AppsettingsConfigurationSource>();
            services.AddTransient<IApiKeyAuthenticationService, ApiKeyAuthenticationService>();
            return services;
        }
    }
}