using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using EcoLogic.ApiKeyAuthentication.KeySources;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;

namespace EcoLogic.ApiKeyAuthentication.Handler
{
    public sealed class ApiKeyAuthenticationService : IApiKeyAuthenticationService
    {
        private readonly IApiKeyAuthenticationSource _apiKeyAuthenticationSource;
        private readonly ILogger<ApiKeyAuthenticationService> _logger;

        public ApiKeyAuthenticationService(ILogger<ApiKeyAuthenticationService> logger, IApiKeyAuthenticationSource apiKeyAuthenticationSource)
        {
            _apiKeyAuthenticationSource = apiKeyAuthenticationSource;
            _logger = logger;
        }

        private const string MissingAuthHeaderMessage = "Authorization Header not found";
        private const string MalformedAuthHeaderMessage = "Authorization Header malformed";
        private const string LoginFailedMessage = "Login Failed";

        public async Task<AuthenticateResult> HandleAuthentication(StringValues? authorizationHeader)
        {
            _logger.LogDebug("Handling ApiKey-Authentication...");
            if (!authorizationHeader.HasValue)
            {
                _logger.LogDebug(MissingAuthHeaderMessage);
                return AuthenticateResult.Fail(MissingAuthHeaderMessage);
            }

            if (authorizationHeader.Value.Count == 0)
            {
                _logger.LogDebug(MissingAuthHeaderMessage);
                return AuthenticateResult.Fail(MissingAuthHeaderMessage);
            }

            var authorizationHeaderValue = authorizationHeader.ToString();
            if (string.IsNullOrWhiteSpace(authorizationHeaderValue))
            {
                _logger.LogDebug(MissingAuthHeaderMessage);
                return AuthenticateResult.Fail(MissingAuthHeaderMessage);
            }

            var token = authorizationHeaderValue
                .Replace(ApiKeyAuthenticationHandler.AuthenticationScheme, string.Empty)
                .Replace("Basic", "")
                .Trim();

            string[] authorization;
            try
            {
                authorization = Encoding.UTF8.GetString(Convert.FromBase64String(token)).Split(':');
            }
            catch (Exception)
            {
                _logger.LogDebug(MalformedAuthHeaderMessage);
                return AuthenticateResult.Fail(MalformedAuthHeaderMessage);
            }

            if (!authorization.Any())
            {
                _logger.LogDebug(MalformedAuthHeaderMessage);
                return AuthenticateResult.Fail(MalformedAuthHeaderMessage);
            }

            if (authorization.Length != 2)
            {
                _logger.LogDebug(MalformedAuthHeaderMessage);
                return AuthenticateResult.Fail(MalformedAuthHeaderMessage);
            }

            var userIdFromRequest = authorization[0];
            var apiKeyFromRequest = authorization[1];
            var apiUserKeyCombination = await _apiKeyAuthenticationSource.FindByNameAsync(userIdFromRequest);
            if (apiUserKeyCombination == null)
            {
                _logger.LogDebug($"User with Id: {userIdFromRequest} was not found.");
                return AuthenticateResult.Fail(LoginFailedMessage);
            }

            if (!apiUserKeyCombination.ApiKey.Equals(apiKeyFromRequest))
            {
                _logger.LogDebug($"Login for User with Id: {userIdFromRequest} failed.");
                return AuthenticateResult.Fail(LoginFailedMessage);
            }

            // create a new claims identity and return an AuthenticationTicket with the correct scheme
            var principal = new ClaimsPrincipal(
                new ClaimsIdentity(new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, apiUserKeyCombination.UserId)
                    },
                    ApiKeyAuthenticationHandler.AuthenticationScheme)
            );
            var ticket = new AuthenticationTicket(principal, new AuthenticationProperties(), ApiKeyAuthenticationHandler.AuthenticationScheme);
            return AuthenticateResult.Success(ticket);
        }
    }
}