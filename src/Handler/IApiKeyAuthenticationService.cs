using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Primitives;

namespace EcoLogic.ApiKeyAuthentication.Handler
{
    public interface IApiKeyAuthenticationService
    {
        Task<AuthenticateResult> HandleAuthentication(StringValues? authorizationHeader);
    }
}