using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EcoLogic.ApiKeyAuthentication.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace EcoLogic.ApiKeyAuthentication.KeySources
{
    public sealed class AppsettingsConfigurationSource : IApiKeyAuthenticationSource
    {
        private readonly ILogger<AppsettingsConfigurationSource> _logger;
        private readonly List<ApiUserKeyCombination> _apiUserKeyCombinations;

        public AppsettingsConfigurationSource(IOptions<List<ApiUserKeyCombination>> apiUserKeyCombinations, ILogger<AppsettingsConfigurationSource> logger)
        {
            _logger = logger;
            _apiUserKeyCombinations = apiUserKeyCombinations.Value;
            _logger.LogDebug($"Loaded {_apiUserKeyCombinations.Count} ApiUserKeyCombinations from AppSettings.");
        }

        public Task<ApiUserKeyCombination> FindByNameAsync(string userId)
        {
            var result = _apiUserKeyCombinations.FirstOrDefault(a => a.UserId == userId);
            if (result == null)
            {
                _logger.LogDebug($"No User found for Id {userId} in Appsettings.");
            }

            return Task.FromResult(result);
        }
    }
}