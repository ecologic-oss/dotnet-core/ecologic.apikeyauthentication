using System.Threading.Tasks;
using EcoLogic.ApiKeyAuthentication.Models;

namespace EcoLogic.ApiKeyAuthentication.KeySources
{
    public interface IApiKeyAuthenticationSource
    {
        Task<ApiUserKeyCombination> FindByNameAsync(string userId);
    }
}