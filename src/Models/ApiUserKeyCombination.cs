namespace EcoLogic.ApiKeyAuthentication.Models
{
    public sealed class ApiUserKeyCombination
    {
        public string UserId { get; set; }
        public string ApiKey { get; set; }
    }
}