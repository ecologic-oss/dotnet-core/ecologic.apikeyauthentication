using EcoLogic.ApiKeyAuthentication.Handler;
using EcoLogic.ApiKeyAuthentication.KeySources;
using EcoLogic.ApiKeyAuthentication.Models;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Xunit;

namespace EcoLogic.ApiKeyAuthentication.Tests
{
    public class ApiKeyAuthenticationHandlerTests
    {
        private readonly Mock<IApiKeyAuthenticationSource> _apiKeyAuthenticationSourceMock;
        private const string MissingAuthHeaderMessage = "Authorization Header not found";
        private const string MalformedAuthHeaderMessage = "Authorization Header malformed";
        private const string LoginFailedMessage = "Login Failed";

        public ApiKeyAuthenticationHandlerTests()
        {
            _apiKeyAuthenticationSourceMock = new Mock<IApiKeyAuthenticationSource>();
            _apiKeyAuthenticationSourceMock.Setup(m => m.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApiUserKeyCombination
            {
                UserId = "wiki",
                ApiKey = "pedia"
            });
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        public async void HandleAuthenticateAsync_ShouldThrowException_IfNoAuthHeaderIsPresent(string authorizationHeaderString)
        {
            // Arrange
            var apiKeyAuthenticationService = new ApiKeyAuthenticationService(new NullLogger<ApiKeyAuthenticationService>(), _apiKeyAuthenticationSourceMock.Object);

            // Act
            var result = await apiKeyAuthenticationService.HandleAuthentication(authorizationHeaderString);

            // Assert
            Assert.False(result.Succeeded);
            Assert.Equal(MissingAuthHeaderMessage, result.Failure.Message);
        }

        [Theory]
        [InlineData("nonBase64String")]
        [InlineData("Basic nonBase64String")]
        public async void HandleAuthenticateAsync_ShouldThrowException_IfAuthHeaderIsMalformed(string authorizationHeaderString)
        {
            // Arrange
            var apiKeyAuthenticationService = new ApiKeyAuthenticationService(new NullLogger<ApiKeyAuthenticationService>(), _apiKeyAuthenticationSourceMock.Object);

            // Act
            var result = await apiKeyAuthenticationService.HandleAuthentication(authorizationHeaderString);

            // Assert
            Assert.False(result.Succeeded);
            Assert.Equal(MalformedAuthHeaderMessage, result.Failure.Message);
        }

        [Theory]
        // user:pwd
        [InlineData("Basic dXNlcjpwd2Q=")]
        public async void HandleAuthenticateAsync_ShouldThrowException_IfUserIsNotFound(string authorizationHeaderString)
        {
            // Arrange
            var apiKeyAuthenticationService = new ApiKeyAuthenticationService(new NullLogger<ApiKeyAuthenticationService>(), _apiKeyAuthenticationSourceMock.Object);

            // Act
            var result = await apiKeyAuthenticationService.HandleAuthentication(authorizationHeaderString);

            // Assert
            Assert.False(result.Succeeded);
            Assert.Equal(LoginFailedMessage, result.Failure.Message);
        }

        [Theory]
        // wiki:pedestrian
        [InlineData("Basic d2lraTpwZWRlc3RyaWFu")]
        public async void HandleAuthenticateAsync_ShouldThrowException_IfPasswordIsInvalid(string authorizationHeaderString)
        {
            // Arrange
            var apiKeyAuthenticationService = new ApiKeyAuthenticationService(new NullLogger<ApiKeyAuthenticationService>(), _apiKeyAuthenticationSourceMock.Object);

            // Act
            var result = await apiKeyAuthenticationService.HandleAuthentication(authorizationHeaderString);

            // Assert
            Assert.False(result.Succeeded);
            Assert.Equal(LoginFailedMessage, result.Failure.Message);
        }

        [Theory]
        // wiki:pedia
        [InlineData("Basic d2lraTpwZWRpYQ==")]
        public async void HandleAuthenticateAsync_ShouldReturnSuceededResult_IfPasswordIsValid(string authorizationHeaderString)
        {
            // Arrange
            var apiKeyAuthenticationService = new ApiKeyAuthenticationService(new NullLogger<ApiKeyAuthenticationService>(), _apiKeyAuthenticationSourceMock.Object);

            // Act
            var result = await apiKeyAuthenticationService.HandleAuthentication(authorizationHeaderString);

            // Assert
            Assert.True(result.Succeeded);
        }
    }
}